<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Categories_Request;
use App\Categories;

class Categories_Controller extends Controller
{
    public function getAdd()
    {
        $data = Categories::select('id','name','parent_id')->get()->toArray();
    	return view('admin.build.cate.add',compact('data'));
    }
    public function postAdd(Categories_Request $request)
    {
        $cate            = new Categories;
        $cate->name      = $request->name;
        $cate->alias     = changeTitle($request->name);
        $cate->parent_id = $request->parent_id;
		$cate->save();
		return redirect()->route('admin.cate.list')->with(['level'=>'success','flash_message'=>'Succesfully!! Created Category']);
    }
    public function getList()
    {
        $data   = Categories::select('id','name','parent_id')->orDerBy('id','DESC')->get()->toArray();
    	return view('admin.build.cate.list',compact('data'));
    }
     public function getEdit($id){
        $data   = Categories::findorFail($id)->toArray();
        $parent = Categories::select('id','name','parent_id')->get()->toArray();
        return view('admin.build.cate.update',compact('data','parent'));
    }
    public function postEdit(Request $request,$id){
        $this->validate($request, ['Name' => 'required'],['Name.required' => 'Please enter name!']);
        $cate            = Categories::find($id);
        $cate->name      = $request->Name;
        $cate->alias     = changeTitle($request->Name);
        $cate->parent_id = $request->Parent_id;
        $cate->save();
        return redirect()->route('admin.cate.list')->with(['flash_message'=>'Succesfully!! Completed update Category','level'=>'success']);
    }
    public function getDelete($id){
        $parent = Categories::where('parent_id',$id)->count();
        if ($parent == 0) {
            $cate = Categories::find($id);
            $cate->delete();
            return redirect()->route('admin.cate.list');
        }
        else{
            echo "<script>
                alert('No action delete Category');
                window.location = '"; echo route('admin.cate.list'); echo "';
                </script>";
        }
    }
}
