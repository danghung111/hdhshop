<?php

namespace App\Http\Controllers;

use App\Http\Requests\Products_Request;
use App\Http\Requests\Edit_Product_Request;
use App\Categories;
use App\Products;
use App\Img_Product;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use Request;
use File;

class Products_Controller extends Controller
{
	public function getAdd(){
		$data = Categories::select('id','name','parent_id')->get()->toArray();
		return view('admin.build.product.add',compact('data'));
	}

	public function postAdd(Products_Request $request){
		$product = new Products();
		$input  = Carbon::now()->format('d-m-y-h-i-s');
		$imgname = $request->file('fileToUpload');
		$img = str_slug($request->Name,'_').$input.'-'.$imgname
		  ->getClientOriginalName();
		$product->name = $request->Name;
		$product->alias =  changeTitle($request->Name);
		$product->price = $request->Price;
		$product->quantity = $request->Quantity;
		$product->intro = $request->Intro;
		$product->description = $request->Description;
		$product->category_id = $request->Category_id;
		$product->images = $img;
		$path = 'public/admin/build/upload/img_product/';
		$product->save();
		$product_id = $product->id;
		if (Input::hasFile('fileToUploadImg')) {
			$files = Input::file('fileToUploadImg');
			foreach ($files as $key => $file ) {
				$img_product = new Img_Product();
				if (isset($file)) {
					$img_product->products_id = $product_id;
					$fileinfo = str_slug($request->Name,'_').$input.'-'.$file
					  ->getClientOriginalName();
					$img_product->img = $fileinfo;
					$path_img_product_details = 'public/admin/build/upload/
						img_details/';
					$file->move($path_img_product_details,$fileinfo);
					$img_product->save();
				}
			}
		}
		$imgname->move($path,$img);
		return redirect()->route('admin.product.list')
		  ->with(['level' => 'Successfully',
			'flash_message' => 'Complete create product']);
	}

	public function getList(){
		$data = Products::select('id','name','price','quantity','intro',
			'description','category_id','images')->orderBy('name', 'desc')
		  ->get()->toArray();
		return view('admin.build.product.list',compact('data'));
	}

	public function getDelete($id){
		$product = Products::find($id);
		$product->delete();
		return redirect()->route('admin.product.list');
	}

	public function getEdit($id){
		$img = Img_Product::where('products_id', $id)->get()->toArray();
		$parent = Categories::select('id','name','parent_id')->get()->toArray();
		$data = Products::findorFail($id)->toArray();
		return view('admin.build.product.update',compact('data','parent','img'));
	}

	public function postEdit(Edit_Product_Request $request,$id){
		$product              = Products::find($id);
		$path = 'public/admin/build/upload/img_product/';
		$name_img_curent = Request::input('img_curent');
		$img_curent   = $path.$name_img_curent;
		$input                = Carbon::now()->format('d-m-y-h-i-s');
		if (!empty(Request::file('fileToUpload'))) {
			$imgname         = $request->file('fileToUpload');
			$img             = str_slug($request->Name,'_').$input.'-'.
				$imgname->getClientOriginalName();
			$path            = 'public/admin/build/upload/img_product/';
			$product->images = $img;
			$imgname->move($path,$img);
			if (File::exists($img_curent)) {
				File::delete($img_curent);
			}
		}
		$product->name        = $request->Name;
		$product->alias       = changeTitle($request->Name);
		$product->price       = $request->Price;
		$product->quantity    = $request->Quantity;
		$product->intro       = $request->Intro;
		$product->description = $request->Description;
		$product->category_id = $request->Category_id;

		$product->save();
		if (Input::hasFile('fileToUploadImg')) {
			$files = Input::file('fileToUploadImg');
			foreach ($files as $key => $file ) {
				$img_product = new Img_Product();
				if (isset($file)) {
					$img_product->products_id = $id;
					$fileinfo = str_slug($request->Name,'_').$input.'-'.$file
					  ->getClientOriginalName();
					$img_product->img = $fileinfo;
					$path_img_product_details = 'public/admin/build/upload/img_details/';
					$file->move($path_img_product_details,$fileinfo);
					$img_product->save();
				}
			}
		}
    return redirect()->route('admin.product.list')->with(['flash_message'=>
    		'Succesfully!! Completed update Category','level'=>'success']);
	}

  	public function getDelImg($id){
	    if (Request::ajax()) {
	      $idImg = (int)Request::get('idImg');
	      $img_details = Img_Product::find($idImg);
	      if (!empty($img_details)) {
	        $image = 'public/admin/build/upload/img_details/'.$img_details->img;
	        if (File::exists($image)) {
	          File::delete($image);
	        }
	        $img_details->delete($idImg);
	      }
	      return "ok";
	    }
	}
}
