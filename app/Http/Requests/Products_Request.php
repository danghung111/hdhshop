<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;



class Products_Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "Name" => "required|unique:products,name",
            "Price" => "required",
            "Quantity" => "required",
            "Intro" => "required",
            "Description" => "required",
            "Category_id" => "required",
            // "Images" => "required"
        ];
    }
    public function messages()
    {
        return [
        "Name.required" => "Please enter name product!!",
        "Name.unique" => "Name product alreadly exists!!",
        "Intro.required" => "Please enter intro product!!",
        "Description.required" => "Please enter description product!!",
        "Price.required" => "Please enter price product!!",
        "Quantity.required" => "Please enter quantity product!!",
        "Category_id.required" => "Please selected category product!!",
        // "images.required" => "Please upload imges product!!",
        ];
    }
}
