<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Img_Product extends Model
{
	protected $table = 'img_products';
	protected $fillable = ['products_id','img','created_at'];
}
