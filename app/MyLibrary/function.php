<?php
  function stripUnicode($str){
  	if(!$str) return fasle;
  	$unicode = array(
  		'a' => 'á|ả|à|ạ|ã|â|ấ|ầ|ẩ|ẫ|ậ|ă|ắ|ằ|ẵ|ặ|ẳ',
  		'A' => 'Á|Ả|À|Ạ|Ã|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ|Ă|Ắ|Ằ|Ẵ|Ặ|Ẳ',
  		'd' => 'đ',
  		'D' => 'Đ',
  		'e' => 'é|è|ẽ|ẹ|ẻ|ê|ế|ề|ễ|ệ|ể',
  		'E' => 'É|È|Ẽ|Ẹ|Ẻ|Ê|Ế|Ề|Ễ|Ệ|Ể',
  		'i' => 'í|ì|ĩ|ị|ỉ',
  		'I' => 'Í|Ì|Ĩ|Ị|Ỉ',
  		'o' => 'ó|ò|õ|ọ|ỏ|ơ|ớ|ờ|ỡ|ợ|ở|ô|ố|ồ|ỗ|ộ|ổ',
  		'O' => 'Ó|Ò|Õ|Ọ|Ỏ|Ơ|Ớ|Ờ|Ỡ|Ợ|Ở|Ô|Ố|Ồ|Ỗ|Ộ|Ổ',
  		'u' => 'ú|ù|ũ|ụ|ủ|ư|ứ|ừ|ữ|ự|ử',
  		'U' => 'Ú|Ù|Ũ|Ụ|Ủ|Ư|Ứ|Ừ|Ữ|Ự|Ử',
  		'y' => 'ý|ỳ|ỹ|ỵ|ỷ',
  		'Y' => 'Ý|Ỳ|Ỹ|Ỵ|Ỷ'
     );
  	foreach ($unicode as $khongdau => $codau){
   		// explode tách chuỗi góc thành nhiều chuỗi
   		// Tách chuỗi thành nhiều chuỗi dựa vào dấu '|'
  		$arr=explode("|", $codau);
  		/* str_replace trả về chuỗi hoặc mảng mới
  		 Thay thế toàn bộ mảng $arr trong $str bằng thay thế bằng phần tử tương ứng $khongdau,
  		*/
       $str = str_replace($arr, $khongdau,$str);
     }
     return $str;
   }
   function changeTitle($str){
  	// Trim  Xóa kí tự  nếu không nhập cho $value thì trim sẽ xóa khoảng trắng Trim($string,$value)-->
     $str=trim($str);
  	/*  str_replace('"','',$str)
  		Thay thế toàn bộ ký tự " có trong chuỗi thành không có ký tự ''
  	 */
     $str = str_replace("'",'',$str);
     $str = str_replace('"','',$str);
  	 // gọi hàm xử lý chuỗi để nhận kết quả là các chữ không dấu
     $str = stripUnicode($str);
  	 // Hàm mb_convert_case đổi chữ hoa thành chữ thường hoặc ngược lại
     $str = mb_convert_case($str,MB_CASE_LOWER,'utf-8');
     $str = str_replace(' ','-',$str);
     return $str;
   }
   function getParent($data,$parent = 0, $str='',$parent_select= 0,$check_id = 0) {
  	//get array in $data
     foreach ($data as $value) {
       $id = $value['id'];
       $name = $value['name'];
  			//Condition Check parent_id = $parent
       if ($value['parent_id']==$parent) {
        if($parent_select!=0 && $id === $parent_select){
         echo "<option value='$id' selected='selected'>$str $name</option>";
       }
       elseif($id == $check_id) {
  					//echo $name
         echo "<option hidden>$name</option>";
       }
       else{
         echo "<option value='$id'>$str $name</option>";
       }
  				//Đệ quy Recusive with $id ($id==$parent)
       getParent($data,$id,$str.'--',$parent_select,$check_id);
     }
   }
  }
?>
