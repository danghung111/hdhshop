<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
	protected $table = 'products';
    protected $fillable = ['id','name','alias','price','quantity','intro',
    'description','category_id','images'];
}
