$(document).ready(function() {
  $('#form-validate').validate({
    submit: {
      settings: {
        inputContainer: '.form-group'
      }
    }
  });
});
$('.swal-btn-cancel').on("click", function(e) {
  var self = $(this);
  e.preventDefault();
  swal({
    title: "Are you sure?",
    text: "You will not be able to recover this imaginary file!",
    type: "warning",
    showCancelButton: true,
    confirmButtonClass: "btn-danger",
    confirmButtonText: "Yes, delete it!",
    cancelButtonText: "No, cancel plx!",
    closeOnConfirm: false,
    closeOnCancel: false
  },
  function(isConfirm) {
    if (isConfirm) {
      self.off("click");
      self.click();
      swal({
        title: "Deleted!",
        text: "Your imaginary file has been deleted.",
        type: "success",
        confirmButtonClass: "btn-success"
      });
    } else {
      swal({
        title: "Cancelled",
        text: "Your imaginary file is safe :)",
        type: "error",
        confirmButtonClass: "btn-danger"
      });
    }
  });
});
$(document).ready(function() {
  $("button#delete-image").on("click", function(e) {
    var self = $(this);
    e.preventDefault();
    var url = "http://localhost/hdhshop/admin/product/delimg/";
    var _token = $("form[name='form-validate']").find("input[name='_token']").val();
    var idImg = $(this).parent().parent().parent().parent().find("img").attr("data-id");
    var img = $(this).parent().parent().parent().parent().find("img").attr("src");
    var rid = $(this).parent().parent().parent().parent().find("img").attr("id");
    swal({
      title: "Are you sure?",
      text: "You will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, cancel plx!",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm) {
      if (isConfirm) {
        self.off("click");
        self.click();
        $.ajax({
          url: url + idImg,
          type: 'GET',
          cache: false,
          data: { "_token": _token, "idImg": idImg, "urlHinh": img },
          success: function(data) {
            if (data == "ok") {
              $("#" + rid).remove();
            } else {
              alert("Error!");
            }
          }
        });
        swal({
          title: "Deleted!",
          text: "Your imaginary file has been deleted.",
          type: "success",
          confirmButtonClass: "btn-success"
        });
      } else {
        swal({
          title: "Cancelled",
          text: "Your imaginary file is safe :)",
          type: "error",
          confirmButtonClass: "btn-danger"
        });
      }
    });
  });
});
$(document).ready(function() {
  $('.gallery-item').matchHeight({
    target: $('.gallery-item .gallery-picture')
  });
});
$(document).ready(function() {
  $('.view_image').on('click', function() {
    $('.imagepreview').attr('src', $(this).parent().parent().parent().parent().find('img').attr('src'));
    $('#imagemodal').modal('show');
  });
});
$(".alert").delay(3000).slideUp();
$('#example').DataTable({
    responsive: true
});
