@extends('admin.build.master')
@section('controller','Categories')
@section('action','Add')
@section('content')
@if(count($errors) > 0)
	<div id="hidden" class="alert alert-warning" role="alert">
		@foreach($errors->all() as $error)
	    <strong>Warning!</strong> {!! $error !!}.
	    @endforeach
	</div>
@endif
<section class="card">
	<div class="card-block">
		<div class="row">
			<div class="col-md-6">
				<form id="form-validate" action="{!! route('admin.cate.postAdd') !!}" name="form-validate" method="POST">
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<fieldset class="form-group">
						<label class="form-label">Name Category</label>
						<input name="name"
							   type="text"
							   class="form-control"
							   data-validation="[NOTEMPTY]">
					</fieldset>
					<fieldset class="form-group">
						<label class="form-label">Parent_Id</label>
						<select type="number" name="parent_id" id="inputTxtParent" class="form-control" required="required">
							<option value="0">choose Categories</option>
							 <?php getParent($data); ?>
						</select>
					</fieldset>
					<fieldset class="form-group">
						<button type="submit" name="uSubmit" class="btn btn-success" value="uSubmit">Create</button>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</section>
@stop
