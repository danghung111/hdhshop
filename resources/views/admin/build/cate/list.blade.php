@extends('admin.build.master')
@section('controller','Categories')
@section('action','List')
@section('content')
<a href="{!! URL::route('admin.cate.getAdd') !!}">
	<button type="button" class="btn btn-primary">
		<span class="font-icon font-icon-pencil"><i>Insert Categories</i></span>
	</button>
</a>
<section class="card">
	<div class="card-block">
		<table id="example" class="display table table-striped table-bordered"
			cellspacing="0" width="100%">
			<thead>
			<tr>
				<th width="1">#</th>
				<th>Name</th>
				<th>Category Parent</th>
				<th width='2'>Update</th>
				<th width='2'>Delete</th>
			</tr>
			</thead>
			<tbody>
				<?php $i = 1; ?>
				@foreach($data as $row)
				<tr>
					<td><?php echo $i++; ?></td>
					<td>{!! $row['name'] !!}</td>
					<td>
					@if ($row['parent_id'] == 0)
						{!! "Null Parent Category" !!}
					@else
						<?php $parent = DB::table('categories')
							->where('id',$row["parent_id"])->first();
						echo $parent->name;
						?>
					@endif
					</td>
					<td>
						<a href="{!! URL::route('admin.cate.getEdit',$row['id']) !!}">
							<button type="button" class="btn btn-info">
								<i title="Update" class="fa fa-edit"></i>
							</button>
						</a>
					</td>
					<td>
						<a href="{!! URL::route('admin.cate.getDelete',$row['id']) !!}">
							<button type="button"  class="btn btn-danger swal-btn-cancel">
								<i title="Delete" class="fa fa-trash"></i>
							</button>
						</a>
						</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</section>
@stop
