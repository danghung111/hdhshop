@extends('admin.build.master')
@section('controller','Categories')
@section('action','Edit')
@section('content')
@if(count($errors) > 0)
	<div id="hidden" class="alert alert-warning" role="alert">
		@foreach($errors->all() as $error)
	    <strong>Warning!</strong> {!! $error !!}.
	    @endforeach
	</div>
@endif
	<section class="card">
		<div class="card-block">
			<div class="row">
				<div class="col-md-6">
					<form id="form-validate" action="{!! route('admin.cate.postEdit',
						$data['id']) !!}" name="form-" method="POST">
						<input type="hidden" name="_token" value="{!! csrf_token() !!}">
						<fieldset class="form-group">
							<label class="form-label">Name Category</label>
							<input name="Name" type="text" class="form-control" >
						</fieldset>
						<fieldset class="form-group">
							<label class="form-label">Parent Category</label>
							<select type="number" name="Parent_id" id="" class="form-control">
								<option value="0">Choose Parent Category</option>
								<?php getParent($parent,0,'',$data["parent_id"],$data["id"]); ?>
							</select>
						</fieldset>
						<fieldset class="form-group">
							<button type="submit" class="btn btn-success">Update</button>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</section>
@stop
