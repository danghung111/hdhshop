<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="{{ asset('public/admin/build/img/ms-icon-144x144.png')}}">
	<meta name="theme-color" content="#ffffff">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<title>HDHShop</title>
	<link href="{{ asset('public/admin/build/img/favicon.144x144.png')}}" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('public/admin/build/img/apple-icon-114x114.png')}}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('public/admin/build/img/apple-icon-120x120.png')}}">
	<link rel="apple-touch-icon" sizes="144x144" href="{{ asset('public/admin/build/img/apple-icon-144x144.png')}}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('public/admin/build/img/apple-icon-152x152.png')}}">
	<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/admin/build/img/apple-icon-180x180.png')}}">
	<link rel="apple-touch-icon" sizes="57x57" href="{{ asset('public/admin/build/img/apple-icon-57x57.png')}}">
	<link rel="apple-touch-icon" sizes="60x60" href="{{ asset('public/admin/build/img/apple-icon-60x60.png')}}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('public/admin/build/img/apple-icon-72x72.png')}}">
	<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('public/admin/build/img/apple-icon-76x76.png')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/admin/build/img/favicon-16x16.png')}}">
	<link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('public/admin/build/img/android-icon-192x192.png')}}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/admin/build/img/favicon-32x32.png')}}">
	<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('public/admin/build/img/favicon-96x96.png')}}">
	<link rel="manifest" href="{{ asset('public/admin/build/img/manifest.json')}}">
	<link rel="stylesheet" href="{{ asset('public/admin/build/css/lib/bootstrap-sweetalert/sweetalert.css')}}">
	<link rel="stylesheet" href="{{ asset('public/admin/build/css/separate/vendor/sweet-alert-animations.min.css')}}">
  <link rel="stylesheet" href="{{ asset('public/admin/build/css/lib/bootstrap/bootstrap.min.css')}}">
 	<link rel="stylesheet" href="{{ asset('public/admin/build/css/lib/bootstrap-table/bootstrap-table.min.css')}}">
	<link rel="stylesheet" href="{{ asset('public/admin/build/css/separate/vendor/datatables-net.min.css') }}">
	<link rel="stylesheet" href="{{ asset('public/admin/build/css/lib/datatables-net/datatables.min.css')}}">
  <link rel="stylesheet" href="{{ asset('public/admin/build/css/lib/font-awesome/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{ asset('public/admin/build/css/separate/pages/gallery.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/admin/build/css/main.css')}}">
</head>
	<body class="with-side-menu-addl-full">
		<header class="site-header">
			@include('admin.build.header')
		</header>
			<div class="mobile-menu-left-overlay"></div>
		<nav class="side-menu">
			@include('admin.build.side-menu')
		</nav>
		<div class="page-content">
			<div class="container-fluid">
				<div class="tbl-row">
					<ol class="breadcrumb breadcrumb-simple">
						<h3>@yield('action') @yield('controller')</h3>
						<li><a href="#">HDHshop</a></li>
						<li><a href="#">@yield('controller')</a></li>
						<li class="active"><i class="label label-success">@yield('action')</i></li>
					</ol>
				</div>
				@if(Session::has('flash_message'))
				<div class="tbl-cell">
					<div  style="text-align: center;" class="alert alert-{!! Session::get('level') !!} alert-fill alert-close alert-dismissible fade in">
						{!! Session::get('flash_message') !!}
					</div>
				</div>
				@endif
				@yield('content')
			</div>
		</div>
		<script src="{{ asset('public/admin/build/js/lib/jquery/jquery.min.js')}}"></script>
		<script src="{{ asset('public/admin/build/js/lib/peity/jquery.peity.min.js')}}"></script>
		<script src="{{ asset('public/admin/build/js/lib/tether/tether.min.js')}}"></script>
		<script src="{{ asset('public/admin/build/js/lib/bootstrap/bootstrap.min.js')}}"></script>
		<script src="{{ asset('public/admin/build/js/lib/html5-form-validation/jquery.validation.min.js')}}"></script>
		<script src="{{ asset('public/admin/build/js/lib/bootstrap-sweetalert/sweetalert.min.js')}}"></script>
		<script src="{{ asset('public/admin/build/js/lib/ckeditor/ckeditor.js')}}"></script>
		<script src="{{ asset('public/admin/build/js/lib/input-mask/input-mask-init.js')}}"></script>
		<script src="{{ asset('public/admin/build/js/lib/input-mask/jquery.mask.min.js')}}"></script>
		<script src="{{ asset('public/admin/build/js/lib/match-height/jquery.matchHeight.min.js')}}"></script>
		<script src="{{ asset('public/admin/build/js/lib/datatables-net/datatables.min.js')}}"></script>
		<script src="{{ asset('public/admin/build/js/plugins.js')}}"></script>
		<script src="{{ asset('public/admin/build/js/myscript.js')}}"></script>
		<script src="{{ asset('public/admin/build/js/app.js')}}"></script>
	</body>
<html>
