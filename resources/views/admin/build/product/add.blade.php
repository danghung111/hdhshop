@extends('admin.build.master')
@section('controller','Products')
@section('action','Add')
@section('content')
@if(count($errors)>0)
	<div class="alert-danger">
		@foreach($errors ->all() as $error)
			<button type="button" class="close" data-dismiss="alert"
				aria-hidden="true">&times;</button>
			<strong>Error</strong> {{ $error }} </br>
		@endforeach
	</div>
@endif
<section class="card">
	<div class="card-block">
		<div class="row">
			<div class="col-sm-12">
				<form id="form-validate" id="upload"
					action="{!! route('admin.product.postAdd') !!}"
				  name="form-validate" method="POST", enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<div class="col-sm-9">
						<fieldset class="form-group">
							<label class="form-label">Name</label>
							<input name="Name"
								   type="text"
								   class="form-control"
								   data-validation="[NOTEMPTY]">
						</fieldset>
						<fieldset class="form-group">
								<label class="form-label" for="money-mask-input">Price</label>
								<input  placeholder="Money mask input: 000.000.000.000,00" name="Price" type="text" class="form-control" data-validation="[NOTEMPTY]" >
						</fieldset>
						<fieldset class="form-group">
							<label class="form-label">Quantity</label>
							<input name="Quantity"
								   type="number" required="required"
								   class="form-control"
								   data-validation="[NOTEMPTY]">
						</fieldset>
						<fieldset class="form-group">
							<label class="form-label">Tags</label>
							<input name="Tag"
										placeholder=".tags"
								   type="text" required="required"
								   class="form-control"
								   data-validation="[NOTEMPTY]">
						</fieldset>
						<fieldset class="form-group">
							<label class="form-label">Category</label>
							<select name="Category_id" id="input" class="form-control">
								<?php getParent($data); ?>
							</select>
						</fieldset>
						<fieldset class="form-group">
							<label class="form-label">Images</label>
							<input name="fileToUpload"
								   type="file" required="required"
								   class="form-control"
								   data-validation="[NOTEMPTY]">
						</fieldset>
						<fieldset class="form-group">
							<label class="form-label">Image Product detailt</label>
							<input name="fileToUploadImg[]"
								   type="file"
								   multiple="multiple"
								   class="form-control">
						</fieldset>
						<fieldset class="form-group">
							<label class="form-label">Intro</label>
							<input name="Intro"
								   type="text"
								   class="form-control"
								   data-validation="[NOTEMPTY]">
						</fieldset>
						<fieldset class="form-group">
							<label class="form-label">Description</label>
							<textarea class="ckeditor" name="Description" class="form-control"
							  rows="3" required="required" >
							  </textarea>
						</fieldset>
					</div>
					<div class="col-sm-12">
						<fieldset class="form-group">
							<button type="submit"  class="btn btn-success">Create</button>
						</fieldset>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
@stop
