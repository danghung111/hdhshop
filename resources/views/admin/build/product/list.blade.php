@extends('admin.build.master')
@section('controller','Product')
@section('action','List')
@section('content')
<a href="{!! URL::route('admin.product.getAdd') !!}">
	<button type="button" class="btn btn-primary">
		<span class="font-icon font-icon-pencil"><i>Insert Product</i></span>
	</button>
</a>
	<section class="card">
		<div class="card-block">
			<table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th width="1">#</th>
						<th>Images</th>
						<th>Name</th>
						<th>Price</th>
						<th width="1">Quantity</th>
						<th>Category Name</th>
						<th width="1">Update</th>
						<th width="1">Delete</th>
					</tr>
				</thead>
				<tbody>
				<?php $i = 1; ?>
				<?php $img = '{{$row["images"]}}'; ?>

					@foreach($data as $row)
						<tr>
							<td><?php echo $i++ ?></td>
							<td><img style="width: 100px" src="{{ asset('public/admin/build/
							upload/img_product')}}/{{ $row['images'] }}" />
							</td>
							<td>{{ $row['name'] }}</td>
							<td>{{ $row['price'] }}</td>
							<td>{{ $row['quantity'] }}</td>
							<td>
							<?php $parent = DB::table('categories')
								->where('id',$row["category_id"])->first();
							echo $parent->name;
							?></td>
							<td>
								<a href="{!! URL::route('admin.product.getEdit',$row['id']) !!}">
									<button type="button" class="btn btn-primary">
										<i class="fa fa-edit"></i>
									</button>
								</a>
							</td>
							<td>
								<a href="{!! URL::route('admin.product.getDelete',$row['id']) !!}">
									<button type="button" class="btn btn-danger swal-btn-cancel">
										<i class="fa fa-trash"></i></button>
								</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</section>
@stop
