@extends('admin.build.master')
@section('controller','Product')
@section('action','List')
@section('content')
@if(count($errors) > 0)
  <div id="hidden" class="alert alert-warning" role="alert">
    @foreach($errors->all() as $error)
      <strong>Warning!</strong> {!! $error !!}.
      @endforeach
  </div>
@endif
<section class="card">
	<div class="card-block">
		<div class="row">
			<div class="col-sm-12">
				<form id="form-validate" action="{!! route('admin.product.postEdit',
					$data['id']) !!}" name="form-validate" method="POST"
					enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="col-sm-9">
					<fieldset class="form-group">
						<label class="form-label">Name</label>
						<input name="Name"
								value="{{ old('Name',isset($data) ? $data['name'] : null) }}"
							    type="text"
							    class="form-control"
							    data-validation="[NOTEMPTY]">
					</fieldset>
					<fieldset class="form-group">
						<label class="form-label">Intro</label>
						<input name="Intro"
								value="{{ old('Name',isset($data) ? $data['intro'] : null) }}"
							   type="text"
							   class="form-control"
							   data-validation="[NOTEMPTY]">
					</fieldset>
					<fieldset class="form-group">
						<label class="form-label">Category</label>
						<select name="Category_id" id="input" class="form-control">
							<?php getParent($parent,0,'',$data['category_id']); ?>
						</select>
					</fieldset>
					<fieldset class="form-group">
						<label class="form-label">Price</label>
						<input name="Price"
								value="{{ old('Name',isset($data) ? $data['price'] : null) }}"
							   type="text"
							   class="form-control"
							   data-validation="[NOTEMPTY]">
					</fieldset>
					<fieldset class="form-group">
						<label class="form-label">Quantity</label>
						<input name="Quantity"
							value="{{ old('Name',isset($data) ? $data['quantity'] : null) }}"
							type="number" required="required"
							class="form-control"
							data-validation="[NOTEMPTY]">
					</fieldset>
          <fieldset class="form-group">
              <label class="form-label">Images</label>
            <input name="fileToUpload"
                value="{{ old('Name',isset($data) ? $data['images'] : null) }}"
                 type="file"
                 class="form-control">
            <input type="hidden" name="img_curent" value="{{ old('Name',isset($data) ? $data['images'] : null) }}">
              <section class="box">
                <div class="box-typical-body">
               <div class="gallery col-md-6">
                  <div class="gallery-col" >
                    <article class="gallery-item" >
                      <img name="img_product" class="gallery-picture" src="{{ asset('public/admin/build/upload/img_product/'
            .old('Name',isset($data) ? $data['images'] : null)) }}">
                      <div class="gallery-hover-layout">
                        <div class="gallery-hover-layout-in">
                          <p class="gallery-item-title">You want to do
                            this image</p>
                          <p>{{ old('Name',isset($data) ? $data['name']
                            : null) }}</p>
                          <div class="btn-group">
                            <button title="view image" type="button"
                              class="btn view_image">
                                <i class="font-icon font-icon-picture"></i>
                            </button>
                          </div>
                          <p>
                            {!! 'Update '.
                            \Carbon\Carbon::createFromTimeStamp(
                              strtotime($data['updated_at']))
                              ->diffForHumans();
                            !!}
                          </p>
                        </div>
                      </div>
                    </article>
                  </div>
                  </div>
                </div>
              </section>
          </fieldset>
				</div>
				<div class="col-sm-12">
					<fieldset class="form-group">
					    <label class="form-label">Images Detailt</label>
					    <input type="file" name="fileImagesDetailt[]" class="form-control"
						    data-validation="[NOTEMPTY]" required="required"
						    multiple="multiple" class="form-control">
			        <section class="box-typical box-typical-full-height-with-header">
			          <header class="box-typical-header box-typical-header-bordered">
		                <div class="tbl-row">
	                    <div class="tbl-cell tbl-cell-title">
	                        <h3>Images Product Details</h3>
	                    </div>
		                </div>
			            </header>
			          <div class="box-typical-body">
			         <div class="gallery col-md-12">
								@foreach($img as $key => $list_images)
                  <div style="padding-bottom: 35px" id="{!! 'image'.$key !!}" class="gallery-col col-md-6" >
                    <article class="gallery-item" >
                      <img id="{!! 'image'.$key !!}" class="gallery-picture" src="{!! asset(
                      	'public/admin/build/upload/img_details/'.
          							$list_images["img"]) !!}" alt="img details"
                          data-id="{!! $list_images['id'] !!}">
                      <div class="gallery-hover-layout">
                        <div class="gallery-hover-layout-in">
                          <p class="gallery-item-title">You want to do
                          	this image</p>
                          <p>{{ old('Name',isset($data) ? $data['name']
                          	: null) }}</p>
                          <div class="btn-group">
                            <button title="view image" type="button"
                            	class="btn view_image">
                                <i class="font-icon font-icon-picture"></i>
                            </button>
                            <button title="delete image" type="button"
                            	class="btn" id="delete-image">
                                <i class="font-icon font-icon-trash"></i>
                            </button>
                          </div>
                          <p>
				                    {!!
				                    \Carbon\Carbon::createFromTimeStamp(
				                    	strtotime($list_images['created_at']))
					                    ->diffForHumans();
				                    !!}
                          </p>
                        </div>
                      </div>
                    </article>
                  </div>
			            	@endforeach
			            </div>
			          </div>
			        </section>
            <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <img src="" class="imagepreview" style="width: 100%;" >
                  </div>
                </div>
              </div>
            </div>
					</fieldset>
					<fieldset class="form-group">
						<label class="form-label">Description</label>
						<textarea name="Description" id="textarea" class="ckeditor"
							rows="3" required="required" data-validation="[NOTEMPTY]">
							{{ old('Name',isset($data) ? $data['description'] : null) }}
						</textarea>
					</fieldset>
				</div>
				<div class="col-sm-12">
					<fieldset class="form-group">
						<button type="submit"  class="btn btn-success">Update</button>
					</fieldset>
				</div>
				</form>
			</div>
		</div>
	</div>
</section>
@stop
