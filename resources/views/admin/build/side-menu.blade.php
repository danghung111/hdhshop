<ul class="side-menu-list">
    <li class="purple with-sub">
        <span>
            <i class="font-icon font-icon-comments active"></i>
            <span class="lbl">Messages</span>
        </span>
        <ul>
            <li><a href="messenger.html"><span class="lbl">Messenger</span></a></li>
            <li><a href="chat.html"><span class="lbl">Messages</span><span class="label label-custom label-pill label-danger">8</span></a></li>
            <li><a href="chat-write.html"><span class="lbl">Write Message</span></a></li>
            <li><a href="chat-index.html"><span class="lbl">Select User</span></a></li>
        </ul>
    </li>
    <li class="red">
        <a href="mail.html">
            <i class="font-icon glyphicon glyphicon-send"></i>
            <span class="lbl">Mail</span>
        </a>
    </li>
    <li class="magenta with-sub">
        <span>
            <span class="font-icon font-icon-folder"></span>
            <span class="lbl">Categories</span>
        </span>
        <ul>
            <a href="{!! URL::route('admin.cate.list') !!}"><span class="lbl">List Categories</span></a></li>
            <a href="{!! URL::route('admin.cate.getAdd') !!}"><span class="lbl">Insert Categories</span></a></li>
        </ul>
    </li>
    <li class="magenta with-sub">
        <span>
            <span class="font-icon font-icon-folder"></span>
            <span class="lbl">Product</span>
        </span>
        <ul>
            <a href="{!! URL::route('admin.product.list') !!}"><span class="lbl">List Product</span></a></li>
            <a href="{!! URL::route('admin.product.getAdd') !!}"><span class="lbl">Insert Product</span></a></li>
        </ul>
    </li>
    <li class="blue with-sub">
        <span>
            <i class="font-icon font-icon-user"></i>
            <span class="lbl">Profile</span>
        </span>
        <ul>
            <li><a href="profile.html"><span class="lbl">Version 1</span></a></li>
            <li><a href="profile-2.html"><span class="lbl">Version 2</span></a></li>
        </ul>
    </li>
    <li class="red">
        <a href="contacts.html" class="label-right">
            <i class="font-icon font-icon-contacts"></i>
            <span class="lbl">Contacts</span>
            <span class="label label-custom label-pill label-danger">35</span>
        </a>
    </li>
    <li class="magenta opened">
        <a href="scheduler.html">
            <i class="font-icon font-icon-calend"></i>
            <span class="lbl">Calendar</span>
        </a>
    </li>
    <li class="blue">
        <a href="files.html">
            <i class="font-icon glyphicon glyphicon-paperclip"></i>
            <span class="lbl">File Manager</span>
        </a>
    </li>
    <li class="gold">
        <a href="gallery.html">
            <i class="font-icon font-icon-picture-2"></i>
            <span class="lbl">Gallery</span>
        </a>
    </li>
    <li class="red">
        <a href="project.html">
            <i class="font-icon font-icon-case-2"></i>
            <span class="lbl">Project</span>
        </a>
    </li>
</ul>
