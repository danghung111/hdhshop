<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>HDHShop</title>

    <link href="{{ asset('public/admin/build/img/favicon.144x144.png')}}" rel="apple-touch-icon" type="image/png" sizes="144x144">
    <link href="{{ asset('public/admin/build/img/favicon.114x114.png')}}" rel="apple-touch-icon" type="image/png" sizes="114x114">
    <link href="{{ asset('public/admin/build/img/favicon.72x72.png')}}" rel="apple-touch-icon" type="image/png" sizes="72x72">
    <link href="{{ asset('public/admin/build/img/favicon.57x57.png')}}" rel="apple-touch-icon" type="image/png">
    <link href="{{ asset('public/admin/build/img/favicon.png')}}" rel="icon" type="image/png">
    <link href="{{ asset('public/admin/build/img/favicon.ico')}}" rel="shortcut icon">
    <link rel="stylesheet" href="{{ asset('public/admin/build/css/lib/bootstrap-sweetalert/sweetalert.css')}}">
    <link rel="stylesheet" href="{{ asset('public/admin/build/css/separate/vendor/sweet-alert-animations.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/admin/build/css/separate/pages/login.min.css')}}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="{{ asset('public/admin/build/css/lib/datatables-net/datatables.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/admin/build/css/separate/vendor/datatables-net.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/admin/build/css/lib/font-awesome/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/admin/build/css/lib/bootstrap/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('public/admin/build/css/main.css')}}">
    <link rel="stylesheet" href="{{ asset('public/admin/build/css/lib/bootstrap-table/bootstrap-table.min.css')}}">
</head>
<body class="with-side-menu-addl-full">

       <div class="page-center">
    <div class="page-center-in">
        <div class="container-fluid">
            <form class="sign-box" action="" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="sign-avatar">
                    <img src="{!! URL::asset('public/admin/build/img/avatar.jpg') !!}" alt="">
                </div>
                <header class="sign-title">Sign In</header>
                <div class="form-group">
                    <input type="text" class="form-control" name="email" placeholder="E-Mail"/>
                </div>
                <div class="form-group">
                    <input type="password" name="Password" class="form-control" placeholder="Password"/>
                </div>
                <div class="form-group">
                    <div class="checkbox float-left">
                        <input type="checkbox" id="signed-in"/>
                        <label for="signed-in">Keep me signed in</label>
                    </div>
                    <div class="float-right reset">
                        <a href="reset-password.html">Reset Password</a>
                    </div>
                </div>
                <button type="submit" class="btn btn-rounded">Sign in</button>
                <p class="sign-note">New to our website? <a href="sign-up.html">Sign up</a></p>
                <!--<button type="button" class="close">
                    <span aria-hidden="true">&times;</span>
                </button>-->
            </form>
        </div>
    </div>
</div><!--.page-center-->
        <!-- end side menu -->
    </nav>

    </div><!--.page-content-->

    <script src="{{ asset('public/admin/build/js/lib/jquery/jquery.min.js')}}"></script>
    <script src="{{ asset('public/admin/build/js/lib/tether/tether.min.js')}}"></script>
    <script src="{{ asset('public/admin/build/js/lib/bootstrap/bootstrap.min.js')}}"></script>
    <script src="{{ asset('public/admin/build/js/plugins.js')}}"></script>
    <script src="{{ asset('public/admin/build/js/lib/peity/jquery.peity.min.js')}}"></script>
    <script src="{{ asset('public/admin/build/js/lib/html5-form-validation/jquery.validation.min.js')}}"></script>
    <script src="{{ asset('public/admin/build/js/bootbox.min.js')}}"></script>
    <script src="{{ asset('public/admin/build/js/lib/bootstrap-sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{ asset('public/admin/build/js/lib/datatables-net/datatables.min.js')}}"></script>
    <script src="{{ asset('public/admin/build/js/myscript.js')}}"></script>
    <script src="{{ asset('public/admin/build/js/app.js')}}"></script>


</body>
</html>
