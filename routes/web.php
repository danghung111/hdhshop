<?php
	Route::get('/', function () {
	    return view('welcome');
	});

	Route::group(['prefix' => 'admin'], function() {
		Route::get('login','Controller@login');
		Route::group(['prefix' => 'cate'], function() {
			Route::get('add',['as'=>'admin.cate.getAdd',
				'uses'=>'Categories_Controller@getAdd']);
			Route::post('postAdd',['as'=>'admin.cate.postAdd',
			  'uses'=>'Categories_Controller@postAdd']);
			Route::get('list',['as'=> 'admin.cate.list',
				'uses'=>'Categories_Controller@getList']);
			Route::get('edit/{id}',['as'=> 'admin.cate.getEdit',
				'uses'=>'Categories_Controller@getEdit']);
			Route::post('postEdit/{id}',['as'=>'admin.cate.postEdit',
				'uses'=>'Categories_Controller@postEdit']);
			Route::get('delete/{id}',['as'=> 'admin.cate.getDelete',
				'uses'=>'Categories_Controller@getDelete']);
		});
		Route::group(['prefix'=>'product'], function(){
			Route::get('add',['as' => 'admin.product.getAdd',
				'uses' => 'Products_Controller@getAdd']);
			Route::post('postAdd',['as' => 'admin.product.postAdd',
				'uses' => 'Products_Controller@postAdd']);
			Route::get('list',['as' => 'admin.product.list',
				'uses' => 'Products_Controller@getList']);
			Route::get('delete/{id}',['as' => 'admin.product.getDelete',
				'uses' => 'Products_Controller@getDelete']);
			Route::get('edit/{id}',['as' => 'admin.product.getEdit',
				'uses' => 'Products_Controller@getEdit']);
			Route::post('postEdit/{id}',['as' => 'admin.product.postEdit',
				'uses' => 'Products_Controller@postEdit']);
			Route::get('delimg/{id}', ['as' =>'admin.product.getDelImg',
				'uses' => 'Products_Controller@getDelImg']);
		});

	});
	Route::any('{all?}', 'Controller@index')->where('all','(.*)');

